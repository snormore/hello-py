const http = require('http');

const name = 'hello-js';
const port = process.env.PORT || '80';

const app = new http.Server();

app.on('request', (req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.write(`Hello! you requested ${req.url}`);
  res.end('\n');
});

app.listen(port, () => {
  console.log(`${name} is listening on port ${port}`);
});

process.on('SIGINT', function() {
  process.exit();
});
